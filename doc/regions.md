---
modulename: Vultr
title: /regions/
giturl: gitlab.com/machines-sh/vultr
editurl: /edit/master/doc/regions.md
weight: 200
---
# Vultr module: Manage regions

## list

List all different regions available for service.


### Example

```sh
space -e APIKEY=01234567DEADBEEF /v1/regions/list/
```

Exit status code is expected to be 0 on success.
