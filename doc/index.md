---
modulename: Vultr
title: Overview
giturl: gitlab.com/machines-sh/vultr
editurl: /edit/master/doc/index.md
weight: 100
---
# Vultr module

_Vultr_ module takes care of interacting with the _Vultr.com_ _API_.


## Dependencies

Modules:  
+ None  

External:  
+ curl  
