---
modulename: Vultr
title: /account/
giturl: gitlab.com/machines-sh/vultr
editurl: /edit/master/doc/account.md
weight: 200
---
# Vultr module: Get account information

Get account information, including balance and information about the last payment operations.


## Example

Retrieve account information:
```sh
space -e APIKEY=01234567DEADBEEF /v1/account/info/
```
Exit status code is expected to be 0 on success.
