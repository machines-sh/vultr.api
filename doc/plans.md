---
modulename: Vultr
title: /plans/
giturl: gitlab.com/machines-sh/vultr
editurl: /edit/master/doc/plans.md
weight: 200
---
# Vultr module: Plans

## list

List all plan offerings.


### Example

Retrieve all offers:
```sh
space -e APIKEY=01234567DEADBEEF /v1/plans/list/
```

Retrieve all Vultr Cloud Computer offers:
```sh
space -e APIKEY=01234567DEADBEEF /v1/plans/list_vc2/
```


Exit status code is expected to be 0 on success.
