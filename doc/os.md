---
modulename: Vultr
title: /os/
giturl: gitlab.com/machines-sh/vultr
editurl: /edit/master/doc/os.md
weight: 200
---
# Vultr module: Operating Systems

## list

List all Operating Systems available for use.


### Example

```sh
space -e APIKEY=01234567DEADBEEF /v1/os/list/
```

Exit status code is expected to be 0 on success.
