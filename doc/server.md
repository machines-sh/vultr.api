---
modulename: Vultr
title: /server/
giturl: gitlab.com/machines-sh/vultr
editurl: /edit/master/doc/server.md
weight: 200
---
# Vultr module: Manage servers

## create

Create a new virtual machine

### Example

Create a new Debian Jessie server, with the most basic configuration (plan), located in Frankfurt:
```sh
space -e APIKEY=01234567DEADBEEF /v1/server/create/ -e OSID=194 -e DCID=9  -e VPSPLANID=201
```

Exit status code is expected to be 0 on success.


## destroy

Destroy an existing virtual machine.

### Example

```sh
space -e APIKEY=01234567DEADBEEF /v1/server/destroy/ -e SUBID=01234567
```

Exit status code is expected to be 0 on success.


