VULTR_API_V1_CALL()
{
    SPACE_DEP="PRINT"
    SPACE_ENV="URLPREFIX URL APIKEY GET=\"${GET-}\" POST=\"${POST-}\" DELETE=\"${DELETE-}\" ${POST-}"

    local method=
    local data=
    if [ -n "${POST}" ]; then
        local key=
        local value=
        for key in ${POST}; do
            value=$(eval echo \$${key})
            #printf "%s=%s\n" "$key" "$value"
            data="${data} --data '${key}=${value}'"
        done
    fi

    local _curl="curl -f -s -S ${URLPREFIX}${URL} -H 'API-Key: ${APIKEY}'${method}${data}"
    PRINT "${_curl}"

    local response=
    local status=
    response=$(eval ${_curl} 2>&1)
    status="$?"
    if [ "${status}" -gt 0 ]; then
        PRINT "Failed talking to API: ${response}." "error"
    else
        if [ -n "${response}" ]; then
            printf "%s\n" "${response}"
        fi
    fi
}
